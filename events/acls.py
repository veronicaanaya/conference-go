from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests

def get_picture_url(query):

    url = f"https://api.pexels.com/v1/search?query={query}"

    headers = {
        "Authorization": PEXELS_API_KEY
    }

    response = requests.get(url, headers=headers)
    api_dict = response.json()
    return api_dict['photos'][0]['src']['original']


def get_weather_data(city, state):
    parameters = {
        "q": f"{city}, {state}, US",
        "appid": OPEN_WEATHER_API_KEY
    }

    url = f"http://api.openweathermap.org/geo/1.0/direct"


    response = requests.get(url, params=parameters)
    content = response.json() #dictionary

    lat = content[0]['lat']
    lon = content[0]['lon']

    params = {
        "lat": lat,
        "lon": lon,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    weather_url = f"https://api.openweathermap.org/data/2.5/weather"

    response = requests.get(weather_url, params=params)
    content = response.json()

    try:
        weather = {
        "description": content["weather"][0]["description"],
        "temp": content["main"]["temp"]
    }
    except:
        return None
    return weather
